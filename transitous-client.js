// SPDX-FileCopyrightText: 2024 Volker Krause <vkrause@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

const Line = {
    Mode: {
        Unknown: undefined,
        Air: 'Air',
        Bus: 'Bus',
        Coach: 'Coach',
        Ferry: 'Ferry',
        LocalTrain: 'LocalTrain',
        LongDistanceTrain: 'LongDistanceTrain',
        Metro: 'Metro',
        RapidTransit: 'RapidTransit',
        Train: 'Train',
        Tram: 'Tram'
    }
};

const Transitous = (function() {
    function parseClasz(clasz) {
        switch (clasz) {
            case 0:
                return Line.Mode.Air;
            case 1:
            case 2:
            case 4:
                return Line.Mode.LongDistanceTrain;
            case 3:
                return Line.Mode.Coach;
            case 5:
            case 6:
                return Line.Mode.LocalTrain;
            case 7:
                return Line.Mode.RapidTransit;
            case 8:
                return Line.Mode.Metro;
            case 9:
                return Line.Mode.Tram;
            case 10:
                return Line.Mode.Bus;
            case 11:
                return Line.Mode.Ferry;
        }
        return Line,Mode.Unknown;
    }

    async function queryStopover(req) {
        const headers = new Headers();
        headers.set("Content-Type", "application/json");
        headers.set("User-Agent", "Transitous JS Client");

        const body = {
            destination: {
                type: "Module",
                target: "/railviz/get_station"
            },
            content_type: "RailVizStationRequest",
            content: {
                time: Math.round(req.time.getTime() / 1000),
                direction: "LATER",
                station_id: req.stopId,
                event_count: req.maximumResults,
                by_schedule_time: false
            }
        };

        const response = await fetch("https://routing.spline.de/api/", {
            method: 'POST',
            body: JSON.stringify(body),
            headers: headers
        });
        const data = await response.json();

        const station = {
            name: data.content.station.name,
            latitude: data.content.station.pos.lat,
            longitude: data.content.station.pos.lng
        };

        const events = data.content.events;

        let results = [];
        for (const ev of events) {
            if (!ev.event.valid) {
                continue;
            }
            let result = results.find((r) => r._id === ev.trips[0].id.id);
            if (!result) {
                result = {
                    _id: ev.trips[0].id.id,
                };
                results.push(result);
            }

            result.stopPoint = station;

            if (ev.type == "DEP") {
                result.scheduledDepartureTime = new Date(ev.event.schedule_time * 1000);
                if (ev.event.reason != "SCHEDULE" && ev.event.reason != "REPAIR") {
                    result.expectedDepartureTime = new Date(ev.event.time * 1000);
                }
            } else if (ev.type == "ARR") {
                result.scheduledArrivalTime = new Date(ev.event.schedule_time * 1000);
                if (ev.event.reason != "SCHEDULE" && ev.event.reason != "REPAIR") {
                    result.expectedArrivalTime = new Date(ev.event.time * 1000);
                }
            }

            result.scheduledPlatform = ev.event.schedule_track;
            if (ev.reason != "SCHEDULE") {
                result.expectedPlatform = ev.event.track;
            }

            result.route = {
                direction: ev.trips[0].transport.direction,
                line: {
                    mode: parseClasz(ev.trips[0].transport.clasz),
                    name: ev.trips[0].transport.name,
                    operatorName: ev.trips[0].transport.provider,
                    color: ev.trips[0].transport.route_color,
                    textColor: ev.trips[0].transport.route_text_color,
                }
            };
        }

        results = results.filter((stop) => stop.scheduledDepartureTime);
        results.sort((lhs, rhs) => lhs.scheduledDepartureTime.getTime() - rhs.scheduledDepartureTime.getTime());
        return results;
    }

    return { queryStopover: queryStopover };
})();
